#include "libpq-fe.h"
#include "iostream"
#include "iomanip"

int main()
{
  PGPing Ping;
  PGconn *connect;
  ConnStatusType status;
  PGresult *result;
  ExecStatusType status_return;
  Ping = PQping("host=postgresql.bts-malraux72.net port=5432");

  if(Ping == PQPING_OK)
  {
    std::cout << "La connexion au serveur de base de données à été établie avec succès" << std::endl;
    connect = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=r.laroy user=r.laroy connect_timeout=10");
    status = PQstatus(connect);

    if(status == CONNECTION_OK)
    {
      char *host = PQhost(connect);
      char *user = PQuser(connect);
      char *pass = PQpass(connect);
      char *dbname = PQdb(connect);
      char *port = PQport(connect);
      int ssl = PQsslInUse(connect);
      const char *param_encodage = "server_encoding";
      const char *encodage = PQparameterStatus(connect, param_encodage);
      int protocol_version = PQprotocolVersion(connect);
      int server_version = PQserverVersion(connect);
      int client_version = PQlibVersion();
      std::string pass_cache;
      std::string ssl_return = "false";

      for(int i = 0; pass[i] != '\0'; i++)
      {
        pass_cache.append("*");
      }

      if(ssl == 1)
      {
        ssl_return = "true";
      }

      std::cout << "La connexion au serveur de base de données " << host << " a été établie avec les paramètres suivants : " << std::endl;
      std::cout << " * utilisateur : " << user  << std::endl;
      std::cout << " * mot de passe : " << pass_cache << std::endl;
      std::cout << " * base de données : " << dbname  << std::endl;
      std::cout << " * port TCP : " << port << std::endl;
      std::cout << " * chiffrement SSL : " << ssl_return << std::endl;
      std::cout << " * encodage : " << encodage  << std::endl;
      std::cout << " * version du protocole : " << protocol_version << std::endl;
      std::cout << " * version du serveur : " << server_version  << std::endl;
      std::cout << " * version de la bibliothèque 'libpq' du client : " << client_version  << std::endl;
    }

    result = PQexec(connect, "SELECT \"Animal\".\"id\" ,\"Animal\".\"nom\" as \"nom de l'animal\", \"Animal\".\"sexe\", \"Animal\".\"date_naissance\" as \"date de naissance\", \"Animal\".\"commentaires\", \"Race\".\"nom\" as \"race\", \"Race\".\"description\" FROM \"si6\".\"Animal\" INNER JOIN \"Race\" ON \"Animal\".\"race_id\" = \"Race\".\"id\" WHERE \"Race\".\"nom\" = 'Singapura' and \"sexe\" = 'Femelle'");
    status_return = PQresultStatus(result);

    if(status_return == PGRES_TUPLES_OK)
    {
      std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)."  << std::endl;
      int nb_lignes = PQntuples(result);
      int nb_colonnes = PQnfields(result);
      const char *separateur = " | ";
      const char *ligne_separation = "-";
      int compteur = 0;
      int compteur1 = 0;
      int maximum = 0;

      for(int i = 0; i < nb_colonnes; i++)
      {
        char *nom_champs = PQfname(result, i);

        for(int l = 0; nom_champs[l] != '\0'; l++)
        {
          compteur++;
        }

        if(compteur > maximum)
        {
          maximum = compteur;
        }

        compteur = 0;
      }

      for(int x = 0; x < nb_colonnes; x++)
      {
        std::cout << separateur << std::setw(maximum) << std::left << PQfname(result, x);
      }

      std::cout << separateur << std::endl << " ";
      int entete = (maximum + 3) * nb_colonnes + 1;

      for(int k = 0; k < entete; k++)
      {
        std::cout << "-";
      }

      std::cout << std::endl;

      for(int i = 0; i < nb_lignes; i++)
      {
        for(int j = 0; j < nb_colonnes; j++)
        {
          char *valeurs_champs = PQgetvalue(result, i, j);

          for(int lo = 0; valeurs_champs[lo] != '\0'; lo++)
          {
            compteur1++;
          }

          if(compteur1 > maximum)
          {
            valeurs_champs[maximum] = '\0';

            for(int lo = maximum - 3; lo < maximum; lo++)
            {
              valeurs_champs[lo] = '.';
            }
          }

          compteur1 = 0;
          std::cout << separateur << std::setw(maximum) << std::left << PQgetvalue(result, i, j);
        }

        std::cout << separateur << std::endl << " ";

        for(int k = 0; k < entete; k++)
        {
          std::cout << "-";
        }

        std::cout << std::endl;
      }

      std::cout << std::endl;
      std::cout << "L'éxécution de la requête SQL à retourné " << nb_lignes  << " enregistrements." << std::endl;
    }
    else if(status_return == PGRES_EMPTY_QUERY)
    {
      std::cout << "La chaîne envoyée au serveur était vide." << std::endl;
    }
    else if(status_return == PGRES_COMMAND_OK)
    {
      std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
    }
    else if(status_return == PGRES_COPY_OUT)
    {
      std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
    }
    else if(status_return == PGRES_COPY_IN)
    {
      std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
    }
    else if(status_return == PGRES_BAD_RESPONSE)
    {
      std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
    }
    else if(status_return == PGRES_NONFATAL_ERROR)
    {
      std::cout << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
    }
    else if(status_return == PGRES_FATAL_ERROR)
    {
      std::cout << "Une erreur fatale est survenue." << std::endl;
    }
    else if(status_return == PGRES_COPY_BOTH)
    {
      std::cout << "" << std::endl;
    }
    else if(status_return == PGRES_SINGLE_TUPLE)
    {
      std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête" << std::endl;
    }
  }
  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << std::endl;
  }
}
